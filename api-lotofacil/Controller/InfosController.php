<?php

require_once '..\autoloadApi.php';
/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 27/06/2017
 * Time: 16:41
 *
 * Classe que tem o objetivo de Pesquisar e manipular os objetos do ipos Infos.
 */
class InfosController
{

    public function listAll(){
        $infos = new InfosTable();
        return ($infos->getALL());
    }

    public function listConcurso()
    {
        $infos = new InfosTable();
        return ($infos->getConcursos());
    }


}