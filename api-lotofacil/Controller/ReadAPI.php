<?php

define(__ROOT__, 'C:\xampp\htdocs\loteriaStats');


require_once __ROOT__ . "/api-lotofacil/model/SorteioJSON.php";

/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 17/05/2017
 * Time: 22:53
 */
class ReadAPI
{

    /**
     * Variavel que armazena os dados do Sorteio lido da API.
     * @var Sorteio
     */
    private $sorteio;

    /**
     * ReadAPI constructor.
     * @param $tipo
     * @param $concurso
     *
     * @return Sorteio;
     */
    public function ReadAPI($tipo, $concurso){
        header("Content-Type: application/json");
        $link = "http://wsloterias.azurewebsites.net/api/sorteio/getresultado/".$tipo."/".$concurso;
        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $jsonDecode = json_decode($output);
        if(! isset($jsonDecode->Status)){
            $this->sorteio = new SorteioJSON();
            $this->sorteio->transformeData($jsonDecode);

            return $this->sorteio;
        }else{
            return false;
        }
    }

    /**
     * @return Sorteio
     */
    public function getSorteio()
    {
        return $this->sorteio;
    }

    /**
     * @param Sorteio $sorteio
     */
    public function setSorteio(Sorteio $sorteio)
    {
        $this->sorteio = $sorteio;
    }

}