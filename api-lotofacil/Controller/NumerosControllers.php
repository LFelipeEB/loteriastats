<?php

require_once '..\autoloadApi.php';
/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 09/07/2017
 * Time: 11:33
 *
 * Classe que tem o objetivo de Pesquisar e manipular os objetos do tipos Numeros.
 */
class NumerosControllers
{
    public function listAll(){
        $numbers = new NumberTable();
        return ($numbers->getALL());
    }

    /**
     * Busca os numeros sorteados no Concurso passado no parametro
     *
     * @param int $concurso Numero do Concurso a ser pesquisado
     */
    public function listByConcurso($concurso){
       $numbers = new NumberTable();
       return($numbers->getByConcurso($concurso));
    }

}