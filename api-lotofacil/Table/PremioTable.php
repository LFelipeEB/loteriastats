<?php

/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 16/06/2017
 * Time: 09:58
 */
class PremioTable
{
    /**
     * @var Premios
     */
    private $premio;

    /**
     * @return Premios
     */
    public function getPremio()
    {
        return $this->premio;
    }

    /**
     * @param Premios $premio
     */
    public function setPremio($premio)
    {
        $this->premio = $premio;
    }

    public function save(Premios $premios){
        $sql = "INSERT INTO premios (concurso, faixa15, faixa14, num_faixa15, num_faixa14) VALUES (".$premios->getConcurso().",".$premios->getFaixa15().",".$premios->getFaixa14().",".$premios->getNumFaixa15().",".$premios->getNumFaixa14().");";

        if($statement = DBConnection::getInstance()->query($sql)){
            return $statement;
        }else{
            return 'erro prepare';
        }
    }

    /**
     * Método para Buscar todas as informações salvas no banco de dados.
     * @return array|bool
     */
    public function getALL(){

        $sql = "SELECT * FROM premios WHERE 1";

        try {
            $statement = DBConnection::getInstance()->prepare($sql);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }catch (PDOException $e){
            return false;
        }
    }

}