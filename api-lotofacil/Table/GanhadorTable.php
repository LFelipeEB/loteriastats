<?php

/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 16/06/2017
 * Time: 09:57
 */
class GanhadorTable
{
    /**
     * @var Ganhador
     */
    private $ganhador;

    /**
     * @var Ganhadores
     */
    private $ganhadores;

    /**
     * Instancia Singleton para manipulação do Banco de Dados;
     * @var PDO;
     */
    private $db;

    /**
     * GanhadorTable constructor.
     * @param Ganhador $ganhador
     * @param Ganhadores $ganhadores
     */
    public function __construct()
    {
        $this->db = DBConnection::getInstance();
    }

    /**
     * @return Ganhador
     */
    public function getGanhador()
    {
        return $this->ganhador;
    }

    /**
     * @param Ganhador $ganhador
     */
    public function setGanhador($ganhador)
    {
        $this->ganhador = $ganhador;
    }

    /**
     * @return Ganhadores
     */
    public function getGanhadores()
    {
        return $this->ganhadores;
    }

    /**
     * @param Ganhadores $ganhadores
     */
    public function setGanhadores($ganhadores)
    {
        $this->ganhadores = $ganhadores;
    }

    public function save(Ganhador $ganhador){
        $sql = "INSERT INTO ganhadores (concurso, cidade, qtd) VALUES (".$ganhador->getConcurso().", '".$ganhador->getCidade()."', ".$ganhador->getQtd().");";
        if($statement = $this->db->query($sql)){
            return $statement;
        }else{
            return 'erro query';
        }

    }

    public function saveGanhadores(Ganhadores $ganhadores)
    {
        if (count($ganhadores->getGanhadores())>0) {
            $sql = "INSERT INTO ganhadores (concurso, cidade, qtd) VALUES ";
            foreach ($ganhadores->getGanhadores() as $ganhador) {
                $sql .= "(" . $ganhador->getConcurso() . ", '" . $ganhador->getCidade() . "', " . $ganhador->getQtd() . "),";
            }
            $sql = substr($sql, 0, -1);
            $sql .= ';';

            if ($statement = $this->db->query($sql)) {
                return $statement;
            } else {
                return 'erro query';
            }
        }
    }

    /**
     * Método para Buscar todas as informações salvas no banco de dados.
     * @return array|bool
     */
    public function getALL(){

        $sql = "SELECT * FROM ganhadores WHERE 1";

        try {
            $statement = DBConnection::getInstance()->prepare($sql);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }catch (PDOException $e){
            return false;
        }
    }
}