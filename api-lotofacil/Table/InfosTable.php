<?php

/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 16/06/2017
 * Time: 09:58
 */
class InfosTable
{

    /**
     * @var Infos
     */
    private $info;

    /**
     * @var PDOStatement
     */
    private $stament;

    /**
     * @return Infos
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param Infos $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @param Infos $infos
     * @return string
     */
    public function save(Infos $infos)
    {

        $sql = "INSERT INTO infos(concurso, acumulou, valor_acumulado, data, realizado, num_sorteios) VALUES (" . $infos->getConcurso() . ", '" . $infos->getAcumulou() . "', " . $infos->getValorAcumulado() . ", '" . $infos->getData() . "', '" . $infos->getRealizado() . "', " . $infos->getNumSorteios() . ");";

        if ($statement = DBConnection::getInstance()->query($sql)) {
            return $statement;
        } else {
            return 'erro prepare';
        }
    }

    public function prepare()
    {
        $sql = "SELECT * FROM infos";
        try {
            $this->stament = DBConnection::getInstance()->prepare($sql);
            return $this->stament;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Metodo deve ser Chamado quando  desejamos passar valores para ser pesquisado na tabela.
     *
     * Ele deve sera chamado Após o metodo prepare e após este método dee ser chamado o execute.
     *
     * @return bool | PDOStatement
     */
    public function where()
    {
        $sql = "SELECT * FROM infos";
        try {
            $this->stament = DBConnection::getInstance()->prepare($sql);
            return $this->stament;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Método para Buscar todas as informações salvas no banco de dados.
     * @return array | bool
     */
    public function getALL()
    {

        $sql = "SELECT * FROM infos WHERE 1";

        try {
            $statement = DBConnection::getInstance()->prepare($sql);
            $statement->execute();
            return ($statement->fetchAll(PDO::FETCH_ASSOC));
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Método para Buscar todos os concursos salvos no banco de dados.
     *
     * @return array | bool  Casa o sistema não consiga fazer a pesquisa Correta é retornado o valor FALSE, obtendo sucesso na presquisa é retornando um array com todos valores.
     */
    public function getConcursos()
    {
        $sql = "SELECT concurso FROM infos";
        try {
            $statement = DBConnection::getInstance()->prepare($sql);
            $statement->execute();
            return ($statement->fetchAll(PDO::FETCH_ASSOC));
        } catch (PDOException $e) {
            return false;
        }

    }
}