<?php

/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 16/06/2017
 * Time: 09:58
 */
class NumberTable
{
    /**
     * @var Numbers
     */
    private $number;

    /**
     * @return Numbers
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param Numbers $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function save(Numbers $numbers){
        $sql = "INSERT INTO number_table (concurso, number1, number2, number3, number4, number5, number6, number7, number8, number9, number10, number11, number12, number13, number14, number15) VALUES (".$numbers->getConcurso().", ".$numbers->getNumber1().", ".$numbers->getNumber2().", ".$numbers->getNumber3().", ".$numbers->getNumber4().", ".$numbers->getNumber5().", ".$numbers->getNumber6().", ".$numbers->getNumber7().", ".$numbers->getNumber8().", ".$numbers->getNumber9().", ".$numbers->getNumber10().", ".$numbers->getNumber11().", ".$numbers->getNumber12().", ".$numbers->getNumber13().", ".$numbers->getNumber14().", ".$numbers->getNumber15().");";

        if($statement = DBConnection::getInstance()->query($sql)){
            return $statement;
        }else{
            return 'erro prepare';
        }
    }

    /**
     * Método para Buscar todas as informações salvas no banco de dados.
     * @return array|bool
     */
    public function getALL(){
        $sql = "SELECT * FROM number_table WHERE 1";

        try {
            $statement = DBConnection::getInstance()->prepare($sql);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }catch (PDOException $e){
            return false;
        }
    }

    /**
     * @param int $concurso Paramentro para ser sdicionado na clausula WHERE.
     */
    public function getByConcurso($concurso)
    {
        $sql = "SELECT * FROM number_table WHERE concurso=".$concurso;

        try {
            $statement = DBConnection::getInstance()->prepare($sql);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }catch (PDOException $e){
            return false;
        }
    }

    /**
     * Método para Buscar todas os numeros salvo no banco de dados.
     * @return array|bool
     */
    public function getALLNumbers(){
        $sql = "
              SELECT
                  number1, number2, number3, number4, number5, number6, number7, number8, number9, number10, number11, number12, number13, number14, number15
              FROM number_table WHERE 1";

        try {
            $statement = DBConnection::getInstance()->prepare($sql);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }catch (PDOException $e){
            return false;
        }
    }

}