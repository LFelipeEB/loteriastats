<?php

define('__ROOT__', 'C:\xampp\htdocs\loteriaStats');

require_once 'controller/ReadAPI.php';
require_once 'controller/InfosController.php';

require_once 'Model/Sorteio.php';
require_once 'Model/Ganhadores.php';
require_once 'Model/Ganhador.php';
require_once 'Model/Infos.php';
require_once 'Model/Numbers.php';
require_once 'Model/Premios.php';

require_once 'Table/GanhadorTable.php';
require_once 'Table/InfosTable.php';
require_once 'Table/NumberTable.php';
require_once 'Table/PremioTable.php';

require_once __ROOT__.'\.db\DBConnection.php';

?>