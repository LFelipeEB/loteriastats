<?php
/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 18/06/2017
 * Time: 18:44
 */
require_once '..\Controller\InfosController.php';
//Chamada das requisições;
//header('Content-Type: json/application; charset=UTF-8');

echo request();

/**
 * Esta função tem o objetivo de fazer a requencia de Requests. Esta função esta tratando toda a requisição e retorno da requisiçõa é feito por ela.
 * @return string
 */
function request()
{
    $infos = new InfosController();

    $isList = isset($_GET['list']) ? $_GET['list'] : false;
    $isAll = isset($_GET['all']) ? $_GET['all'] : false;
    $isConcurso = isset($_GET['concurso']) ? $_GET['concurso'] : false;

    if ($isList) {
        if($isAll) {
            $return = json_encode($infos->listAll());
        }else{
            if ($isConcurso)
                $return = json_encode($infos->listConcurso());
        }
    }

    return $return;
}