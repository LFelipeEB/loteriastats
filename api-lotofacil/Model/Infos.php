<?php


class Infos
{
    private $concurso, $acumulou, $valor_acumulado, $data, $realizado, $num_sorteios, $created, $modified;

    /**
     * Infos constructor.
     * @param $concurso
     * @param $acumulou
     * @param $valor_acumulado
     * @param $data
     * @param $realizado
     * @param $num_sorteios
     * @param $created
     * @param $modified
     */
    public function setAll($concurso, $acumulou, $valor_acumulado, $data, $realizado, $num_sorteios, $created, $modified)
    {
        $this->concurso = $concurso;
        $this->acumulou = $acumulou;
        $this->valor_acumulado = $valor_acumulado;
        $this->data = $data;
        $this->realizado = $realizado;
        $this->num_sorteios = $num_sorteios;
        $this->created = $created;
        $this->modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getConcurso()
    {
        return $this->concurso;
    }

    /**
     * @param mixed $concurso
     */
    public function setConcurso($concurso)
    {
        $this->concurso = $concurso;
    }

    /**
     * @return mixed
     */
    public function getAcumulou()
    {
        return $this->acumulou ? 1 : 0;
    }

    /**
     * @param mixed $acumulou
     */
    public function setAcumulou($acumulou)
    {
        $this->acumulou = $acumulou;
    }

    /**
     * @return mixed
     */
    public function getValorAcumulado()
    {
        return $this->valor_acumulado;
    }

    /**
     * @param mixed $valor_acumulado
     */
    public function setValorAcumulado($valor_acumulado)
    {
        $this->valor_acumulado = $valor_acumulado;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getRealizado()
    {
        return $this->realizado;
    }

    /**
     * @param mixed $realizado
     */
    public function setRealizado($realizado)
    {
        $this->realizado = $realizado;
    }

    /**
     * @return mixed
     */
    public function getNumSorteios()
    {
        return $this->num_sorteios;
    }

    /**
     * @param mixed $num_sorteios
     */
    public function setNumSorteios($num_sorteios)
    {
        $this->num_sorteios = $num_sorteios;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param mixed $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }


    public function setInfosFromSorteio(SorteioJSON $sorteio){
        $this->setAcumulou($sorteio->getAcumulou());
        $this->setConcurso($sorteio->getNumConcurso());
        $this->setData($sorteio->getData());
        $this->setNumSorteios($sorteio->getNumSorteio());
        $this->setRealizado($sorteio->getRealizado());
        $this->setValorAcumulado($sorteio->getValorAcumulado());
    }
}