<?php

/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 19/05/2017
 * Time: 13:08
 */
class Ganhador
{
    private $concurso, $cidade, $qtd, $created, $modified;

    /**
     * @return mixed
     */
    public function getConcurso()
    {
        return $this->concurso;
    }

    /**
     * @param mixed $concurso
     */
    public function setConcurso($concurso)
    {
        $this->concurso = $concurso;
    }

    /**
     * @return mixed
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param mixed $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return mixed
     */
    public function getQtd()
    {
        return $this->qtd;
    }

    /**
     * @param mixed $qtd
     */
    public function setQtd($qtd)
    {
        $this->qtd = $qtd;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param mixed $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

}