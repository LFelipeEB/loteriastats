<?php

require_once __ROOT__ . '\api-lotofacil\autoloadApi.php';

/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 16/06/2017
 * Time: 09:45
 */
class Sorteio
{
    /**
     * @var Ganhadores
     */
    private $ganhadores;

    /**
     * @var Infos
     */
    private $infos;

    /**
     * @var Numbers
     */
    private $numbers;

    /**
     * @var Premios
     */
    private $premios;

    /**
     * @var GanhadorTable
     */
    private $ganhadoresTable;

    /**
     * @var InfosTable
     */
    private $infosTable;

    /**
     * @var NumberTable
     */
    private $numberTable;

    /**
     * @var PremioTable
     */
    private $premioTable;
    /**
     * Sorteio constructor.
     * @param Ganhadores $ganhadores
     * @param Infos $infos
     * @param Numbers $numbers
     * @param Premios $premios
     */
    public function __construct()
    {
        $this->ganhadores = new Ganhadores();
        $this->infos = new Infos();
        $this->numbers = new Numbers();
        $this->premios = new Premios();
    }


    /**
     * @return Ganhadores
     */
    public function getGanhadores()
    {
        return $this->ganhadores;
    }

    /**
     * @param Ganhadores $ganhadores
     */
    public function setGanhadores($ganhadores)
    {
        $this->ganhadores = $ganhadores;
    }

    /**
     * @return Infos
     */
    public function getInfos()
    {
        return $this->infos;
    }

    /**
     * @param Infos $infos
     */
    public function setInfos(Infos $infos)
    {
        $this->infos = $infos;
    }

    /**
     * @return Numbers
     */
    public function getNumbers()
    {
        return $this->numbers;
    }

    /**
     * @param Numbers $numbers
     */
    public function setNumbers(Numbers $numbers)
    {
        $this->numbers = $numbers;
    }

    /**
     * @return Premios
     */
    public function getPremios()
    {
        return $this->premios;
    }

    /**
     * @param Premios $premios
     */
    public function setPremios(Premios $premios)
    {
        $this->premios = $premios;
    }

    /**
     * Podemos Obter os daos atravez da leitura da API.
     *
     * @param SorteioJSON $json
     */
    public function sorteioFromSorteioJSON(SorteioJSON $json){
        $this->ganhadores->setGanhadoresFromSorteio($json);
        $this->premios->setPremiosFromSorteio($json);
        $this->infos->setInfosFromSorteio($json);
        $this->numbers->setNumbersFromSorteio($json);

        return $this;
    }

    public function save(){
        $this->infosTable = new InfosTable();
        $this->ganhadoresTable = new GanhadorTable();
        $this->premioTable = new PremioTable();
        $this->numberTable = new NumberTable();

        $this->infosTable->save($this->infos);
        $this->numberTable->save($this->numbers);
        $this->premioTable->save($this->premios);
        $this->ganhadoresTable->saveGanhadores($this->getGanhadores());

        return $this->getInfos()->getConcurso();
    }

}