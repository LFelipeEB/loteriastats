<?php

/**
 * Class Sorteio
 *
 * Classe responsavel por separar os dados do JSON retornando na API.
 */
class SorteioJSON
{

    private $num_concurso, $acumulou, $est_premio, $valor_acumulado,
        $data, $realizado, $data_proximo, $acumulado_especial, $arrecadado,
        $num_sorteio, $numeros, $premios, $ganhadores;

    public function transformeData($jsonData){
        $this->num_concurso = $jsonData->NumeroConcurso;
        $this->acumulou = $jsonData->Acumulou;
        $this->est_premio = $jsonData->EstimativaPremio;
        $this->valor_acumulado = $jsonData->ValorAcumulado;
        $this->data = $jsonData->Data;
        $this->realizado = $jsonData->RealizadoEm;
        $this->data_proximo = $jsonData->DataProximo;
        $this->acumulado_especial = $jsonData->ValorAcumuladoEspecial;
        $this->arrecadado = $jsonData->Arrecadacao;
        $this->num_sorteio = $jsonData->Sorteios[0]->NumSorteio;
        $this->numeros = $jsonData->Sorteios[0]->Numeros;
        $this->premios = $jsonData->Sorteios[0]->Premios;
        $this->ganhadores = $jsonData->Sorteios[0]->Ganhadores;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumConcurso()
    {
        return $this->num_concurso;
    }

    /**
     * @param mixed $num_concurso
     */
    public function setNumConcurso($num_concurso)
    {
        $this->num_concurso = $num_concurso;
    }

    /**
     * @return mixed
     */
    public function getAcumulou()
    {
        return $this->acumulou;
    }

    /**
     * @param mixed $acumulou
     */
    public function setAcumulou($acumulou)
    {
        $this->acumulou = $acumulou;
    }

    /**
     * @return mixed
     */
    public function getEstPremio()
    {
        return $this->est_premio;
    }

    /**
     * @param mixed $est_premio
     */
    public function setEstPremio($est_premio)
    {
        $this->est_premio = $est_premio;
    }

    /**
     * @return mixed
     */
    public function getValorAcumulado()
    {
        return $this->valor_acumulado;
    }

    /**
     * @param mixed $valor_acumulado
     */
    public function setValorAcumulado($valor_acumulado)
    {
        $this->valor_acumulado = $valor_acumulado;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getRealizado()
    {
        return $this->realizado;
    }

    /**
     * @param mixed $realizado
     */
    public function setRealizado($realizado)
    {
        $this->realizado = $realizado;
    }

    /**
     * @return mixed
     */
    public function getDataProximo()
    {
        return $this->data_proximo;
    }

    /**
     * @param mixed $data_proximo
     */
    public function setDataProximo($data_proximo)
    {
        $this->data_proximo = $data_proximo;
    }

    /**
     * @return mixed
     */
    public function getAcumuladoEspecial()
    {
        return $this->acumulado_especial;
    }

    /**
     * @param mixed $acumulado_especial
     */
    public function setAcumuladoEspecial($acumulado_especial)
    {
        $this->acumulado_especial = $acumulado_especial;
    }

    /**
     * @return mixed
     */
    public function getArrecadado()
    {
        return $this->arrecadado;
    }

    /**
     * @param mixed $arrecadado
     */
    public function setArrecadado($arrecadado)
    {
        $this->arrecadado = $arrecadado;
    }

    /**
     * @return mixed
     */
    public function getNumSorteio()
    {
        return $this->num_sorteio;
    }

    /**
     * @param mixed $num_sorteio
     */
    public function setNumSorteio($num_sorteio)
    {
        $this->num_sorteio = $num_sorteio;
    }

    /**
     * @return mixed
     */
    public function getNumeros()
    {
        return $this->numeros;
    }

    /**
     * @param mixed $numeros
     */
    public function setNumeros($numeros)
    {
        $this->numeros = $numeros;
    }

    /**
     * @return mixed
     */
    public function getPremios()
    {
        return $this->premios;
    }

    /**
     * @param mixed $premios
     */
    public function setPremios($premios)
    {
        $this->premios = $premios;
    }

    /**
     * @return mixed
     */
    public function getGanhadores()
    {
        return $this->ganhadores;
    }

    /**
     * @param mixed $ganhadores
     */
    public function setGanhadores($ganhadores)
    {
        $this->ganhadores = $ganhadores;
    }


}