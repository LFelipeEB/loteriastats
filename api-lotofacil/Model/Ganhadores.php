<?php

/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 21/05/2017
 * Time: 22:31
 */
class Ganhadores
{
    private $ganhadores;

    /**
     * Ganhadores constructor.
     * @param $ganhadores
     */
    public function __construct()
    {
        $this->ganhadores = array();
    }

    /**
     * @return mixed
     */
    public function getGanhadores()
    {
        return $this->ganhadores;
    }

    /**
     * @param mixed $ganhadores
     */
    public function setGanhadores($ganhadores)
    {
        $this->ganhadores = $ganhadores;
    }

    public function getGanhador($num){
        return $this->ganhadores[$num];
    }

    public function setGanhadoresFromSorteio(SorteioJSON $sorteio){
        foreach ($sorteio->getGanhadores() as $win){
            $winer = new Ganhador();
            $winer->setConcurso($sorteio->getNumConcurso());
            $winer->setCidade($win->CidadeEstado);
            $winer->setQtd($win->Quantidade);
            array_push($this->ganhadores, $winer);
        }

        return $this;
    }



}