<?php

/**
 * Class Premios
 *
 * Está classe é responsavel por armazenas os dados dos premios de 15 e 14 numeros.
 */
class Premios
{
    private $concurso, $faixa15 , $faixa14, $numFaixa14, $numFaixa15;

    /**
     * @return mixed
     */
    public function getNumFaixa14()
    {
        return $this->numFaixa14;
    }

    /**
     * @param mixed $numFaixa14
     */
    public function setNumFaixa14($numFaixa14)
    {
        $this->numFaixa14 = $numFaixa14;
    }

    /**
     * @return mixed
     */
    public function getNumFaixa15()
    {
        return $this->numFaixa15;
    }

    /**
     * @param mixed $numFaixa15
     */
    public function setNumFaixa15($numFaixa15)
    {
        $this->numFaixa15 = $numFaixa15;
    }

    /**
     * @return mixed
     */
    public function getConcurso()
    {
        return $this->concurso;
    }

    /**
     * @param mixed $concurso
     */
    public function setConcurso($concurso)
    {
        $this->concurso = $concurso;
    }

    /**
     * @return mixed
     */
    public function getFaixa15()
    {
        return $this->faixa15;
    }

    /**
     * @param mixed $faixa15
     */
    public function setFaixa15($faixa15)
    {
        $this->faixa15 = $faixa15;
    }

    /**
     * @return mixed
     */
    public function getFaixa14()
    {
        return $this->faixa14;
    }

    /**
     * @param mixed $faixa14
     */
    public function setFaixa14($faixa14)
    {
        $this->faixa14 = $faixa14;
    }

    public function setPremiosFromSorteio(SorteioJSON $sorteio){
        $this->setConcurso($sorteio->getNumConcurso());
        $this->setFaixa14($sorteio->getPremios()[1]->Valor);
        $this->setNumFaixa14($sorteio->getPremios()[1]->NumeroGanhadores);
        $this->setFaixa15($sorteio->getPremios()[0]->Valor);
        $this->setNumFaixa15($sorteio->getPremios()[0]->NumeroGanhadores);
    }
}