<?php

/**
 * Created by PhpStorm.
 * User: Luiz Felipe
 * Date: 17/05/2017
 * Time: 23:40
 */
class Numbers
{
    private $id, $concurso,
        $number1, $number2, $number3, $number4, $number5, $number6, $number7, $number8, $number9, $number10, $number11, $number12,
        $number13, $number14, $number15, $created, $modified;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getConcurso()
    {
        return $this->concurso;
    }

    /**
     * @param mixed $concurso
     */
    public function setConcurso($concurso)
    {
        $this->concurso = $concurso;
    }

    /**
     * @return mixed
     */
    public function getNumber1()
    {
        return $this->number1;
    }

    /**
     * @param mixed $number1
     */
    public function setNumber1($number1)
    {
        $this->number1 = $number1;
    }

    /**
     * @return mixed
     */
    public function getNumber2()
    {
        return $this->number2;
    }

    /**
     * @param mixed $number2
     */
    public function setNumber2($number2)
    {
        $this->number2 = $number2;
    }

    /**
     * @return mixed
     */
    public function getNumber3()
    {
        return $this->number3;
    }

    /**
     * @param mixed $number3
     */
    public function setNumber3($number3)
    {
        $this->number3 = $number3;
    }

    /**
     * @return mixed
     */
    public function getNumber4()
    {
        return $this->number4;
    }

    /**
     * @param mixed $number4
     */
    public function setNumber4($number4)
    {
        $this->number4 = $number4;
    }

    /**
     * @return mixed
     */
    public function getNumber5()
    {
        return $this->number5;
    }

    /**
     * @param mixed $number5
     */
    public function setNumber5($number5)
    {
        $this->number5 = $number5;
    }

    /**
     * @return mixed
     */
    public function getNumber6()
    {
        return $this->number6;
    }

    /**
     * @param mixed $number6
     */
    public function setNumber6($number6)
    {
        $this->number6 = $number6;
    }

    /**
     * @return mixed
     */
    public function getNumber7()
    {
        return $this->number7;
    }

    /**
     * @param mixed $number7
     */
    public function setNumber7($number7)
    {
        $this->number7 = $number7;
    }

    /**
     * @return mixed
     */
    public function getNumber8()
    {
        return $this->number8;
    }

    /**
     * @param mixed $number8
     */
    public function setNumber8($number8)
    {
        $this->number8 = $number8;
    }

    /**
     * @return mixed
     */
    public function getNumber9()
    {
        return $this->number9;
    }

    /**
     * @param mixed $number9
     */
    public function setNumber9($number9)
    {
        $this->number9 = $number9;
    }

    /**
     * @return mixed
     */
    public function getNumber10()
    {
        return $this->number10;
    }

    /**
     * @param mixed $number10
     */
    public function setNumber10($number10)
    {
        $this->number10 = $number10;
    }

    /**
     * @return mixed
     */
    public function getNumber11()
    {
        return $this->number11;
    }

    /**
     * @param mixed $number11
     */
    public function setNumber11($number11)
    {
        $this->number11 = $number11;
    }

    /**
     * @return mixed
     */
    public function getNumber12()
    {
        return $this->number12;
    }

    /**
     * @param mixed $number12
     */
    public function setNumber12($number12)
    {
        $this->number12 = $number12;
    }

    /**
     * @return mixed
     */
    public function getNumber13()
    {
        return $this->number13;
    }

    /**
     * @param mixed $number13
     */
    public function setNumber13($number13)
    {
        $this->number13 = $number13;
    }

    /**
     * @return mixed
     */
    public function getNumber14()
    {
        return $this->number14;
    }

    /**
     * @param mixed $number14
     */
    public function setNumber14($number14)
    {
        $this->number14 = $number14;
    }

    /**
     * @return mixed
     */
    public function getNumber15()
    {
        return $this->number15;
    }

    /**
     * @param mixed $number15
     */
    public function setNumber15($number15)
    {
        $this->number15 = $number15;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param mixed $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    public function setNumbersFromSorteio(SorteioJSON $sorteio){
        $numbers = $sorteio->getNumeros();
        $this->setConcurso($sorteio->getNumConcurso());
        $this->setNumber1($numbers[0]);
        $this->setNumber2($numbers[1]);
        $this->setNumber3($numbers[2]);
        $this->setNumber4($numbers[3]);
        $this->setNumber5($numbers[4]);
        $this->setNumber6($numbers[5]);
        $this->setNumber7($numbers[6]);
        $this->setNumber8($numbers[7]);
        $this->setNumber9($numbers[8]);
        $this->setNumber10($numbers[9]);
        $this->setNumber11($numbers[10]);
        $this->setNumber12($numbers[11]);
        $this->setNumber13($numbers[12]);
        $this->setNumber14($numbers[13]);
        $this->setNumber15($numbers[14]);
        return $this;
    }
}