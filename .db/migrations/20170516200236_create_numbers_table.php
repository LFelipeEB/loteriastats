<?php

use Phinx\Migration\AbstractMigration;

class CreateNumbersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table("number_table");
        $table->addColumn('concurso', 'integer')
            ->addColumn('number1', 'integer')
            ->addColumn('number2', 'integer')
            ->addColumn('number3', 'integer')
            ->addColumn('number4', 'integer')
            ->addColumn('number5', 'integer')
            ->addColumn('number6', 'integer')
            ->addColumn('number7', 'integer')
            ->addColumn('number8', 'integer')
            ->addColumn('number9', 'integer')
            ->addColumn('number10', 'integer')
            ->addColumn('number11', 'integer')
            ->addColumn('number12', 'integer')
            ->addColumn('number13', 'integer')
            ->addColumn('number14', 'integer')
            ->addColumn('number15', 'integer')
            ->addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addColumn('modified', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addForeignKey('concurso', 'infos', 'concurso', array('constraint'=>'fk_number_infos'))
            ->create();
    }
}
