<?php

use Phinx\Migration\AbstractMigration;

class CreatePremios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('premios');
        $table->addColumn('concurso','integer')
            ->addColumn('faixa15', 'integer')
            ->addColumn('num_faixa15', 'integer')
            ->addColumn('faixa14', 'integer')
            ->addColumn('num_faixa14', 'integer')
            ->addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addColumn('modified', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addForeignKey('concurso', 'infos', 'concurso')
           ->create();
    }
}
